const
 app = require('http').createServer(http_server),
 io = require('socket.io')(app),
 fs = require('fs'),
 settings = require('./settings_teleentry.json'),
 Sequelize = require('sequelize');

var HTTP_HOST = "104.131.0.237"
var HTTP_PORT = 8080

if (settings.DEBUG == true) {
  HTTP_HOST = "localhost"
}

// ===================== DATABASE MODELS ( BEGIN ) ===============
const teleentry_db = new Sequelize('tele_entry', 'tele_entry_user', 'tele_entry_password', {
 	host: 'localhost',
 	dialect: 'postgres',
 	pool: {
 		max: 5,
 		min: 0,
 		idle: 10000
 	}
 })

 var Device = teleentry_db.define('device', {
  id: {
    type: Sequelize.STRING,
    field: 'id',
    primaryKey: true
  },
  identifier: {
    type: Sequelize.STRING,
    field: 'identifier'
  },
  api_key: {
    type: Sequelize.STRING,
    field: 'api_key'
  },
  name: {
  	type: Sequelize.STRING,
  	field: 'name'
  }
}, {
  freezeTableName: true,
  timestamps: false
})

var Codes = teleentry_db.define('codes', {
  code: {
    type: Sequelize.STRING,
    field: 'code'
  },
  device: {
    type: Sequelize.UUID,
    field: 'device_id',
    references: {
      model: Device,
      key: 'id'
    }
  },
  eeprom_save: {
    type: Sequelize.BOOLEAN,
    field: 'eeprom_save'
  },
  eeprom_deleted: {
    type: Sequelize.BOOLEAN,
    field: 'eeprom_deleted'
  },
}, {
  freezeTableName: true,
  timestamps: false
})

function find_device(data){
  return Device.findOne({
    where: {
      identifier: data.identifier
    }
  })
}

function find_device_by_id(data){
  return Device.findOne({
    where: {
      id: data
    }
  })
}

function find_code(device_data, data){
  return Codes.findOne({
    where: {
      device: device_data.id,
      code: data.code
    }
  })
}

function find_code_save_false(device_data){
  return Codes.findOne({
    where: {
      device: device_data.id,
      eeprom_save: false
    }
  })
}

// setInterval(check_saved_codes, 5000)
//
// function check_saved_codes(){
//   codes = Codes.all().then(function(data){
//     data.forEach(function(c){
//       console.log(c.dataValues.device, c.dataValues.code, c.dataValues.eeprom_save)
//       if (c.dataValues.eeprom_save === false){
//         find_device_by_id(c.dataValues.device).then(function(device_data){
//           console.log("device identifier", device_data.dataValues.identifier)
//           device_command.emit("SAVE",
//             {identifier: device_data.dataValues.identifier, code: c.dataValues.code})
//         })
//       }
//     })
//   })
// }

// ===================== DATABASE MODELS ( END ) ===============

// ===================== SOCKET.IO ( BEGIN ) ===============

var device_command = io.of('/devicecommand')
  .on('connection', function(socket){

    socket.emit('welcome', {
      msg: 'welcome'
    })

    // socket.on('NEWCODE', function(data){
    //   console.log("NEWCODE", data)
    //   device_command.emit("SAVE", data)
    // })

    socket.on('SAVE_OK', function(data){
      console.log("data saved", data)
      find_device(data).then(
        function(device_data){
          Codes.update({
            eeprom_save: true
          }, {
            where: {
              device: device_data.id,
              code: data.code
            }
          })
        }
      )
    })

    socket.on('CHECK_CODE', function(data, fn){
      console.log("CHECK_CODE", data)
      find_device(data).then(
        function(device_data){

          find_code(device_data, data).then(function(code_data){
            if (code_data !== null){ // code exist
              fn({status: true, code: code_data.dataValues.code})
            }
            else{ // code not found
              fn({status: false, code: data.code})
            }
          })

        })
    })

    socket.on('client_begin_conection', function(data){
      console.log('client_begin_conection', data)
      find_device(data).then(function(device_data){

        find_code_save_false(device_data).then(function(code_data){
          if (code_data !== null){
            device_command.emit("SAVE",
              {identifier: device_data.dataValues.identifier, code: code_data.dataValues.code})
          }
        })

      })
    })

  })

// ===================== SOCKET.IO ( END ) ===============

// ===================== HTTP SERVER ( BEGIN ) ===============

function http_server(req, res){
  res.writeHead(200, {'Content-Type': 'text/html'});

  // =========== Monitor
	if (req.url == '/'){
    fs.readFile(__dirname + '/index.html',
    function (err, data) {
      if (err) {
        res.writeHead(500)
        return res.end('Error loading index.html')
      }

      res.writeHead(200)
      res.end(data)
    })
	}

// ========== Send SAVE to tcp client
	if (req.url == '/send/save/' && req.method == 'POST'){
		var body = ''
		req.on('data', function(chunk){
			body += chunk
		})

		req.on('end', function(){
			var data_to_send = JSON.parse(body)
      console.log('data_to_send', data_to_send)
      device_command.emit("SAVE",
        {identifier: data_to_send.identifier, code: data_to_send.code})
			res.end("OK save")
		})
	}
}

app.listen(HTTP_PORT, HTTP_HOST);
console.log("server listening " + HTTP_HOST + " port: " + HTTP_PORT);
// ===================== HTTP SERVER ( END ) ===============
